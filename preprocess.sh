
PW=${MARIADB_ROOT_PASSWORD}
OUT_DIR="/var/lib/mysql-files"

# Download databases
if [ ! -f "${OUT_DIR}/${VERSION}-ClinicalEvidenceSummaries.tsv" ]
then
  echo "Could not find database files. Starting file download..."
  # Remove former database files
  for FILE in "$OUT_DIR"/*
  do
    echo "Former database file found. Removing file ${FILE}..."
    rm -v "${FILE}"
  done

  wget -v "https://civicdb.org/downloads/${VERSION}/${VERSION}-GeneSummaries.tsv" -P ${OUT_DIR}
  wget -v "https://civicdb.org/downloads/${VERSION}/${VERSION}-VariantSummaries.tsv" -P ${OUT_DIR}
  wget -v "https://civicdb.org/downloads/${VERSION}/${VERSION}-MolecularProfileSummaries.tsv" -P ${OUT_DIR}
  wget -v "https://civicdb.org/downloads/${VERSION}/${VERSION}-ClinicalEvidenceSummaries.tsv" -P ${OUT_DIR}
  wget -v "https://civicdb.org/downloads/${VERSION}/${VERSION}-VariantGroupSummaries.tsv" -P ${OUT_DIR}
  wget -v "https://civicdb.org/downloads/${VERSION}/${VERSION}-AssertionSummaries.tsv" -P ${OUT_DIR}
  wget -v "https://civicdb.org/downloads/${VERSION}/${VERSION}-civic_accepted.vcf" -P ${OUT_DIR}
  wget -v "https://civicdb.org/downloads/${VERSION}/${VERSION}-civic_accepted_and_submitted.vcf" -P ${OUT_DIR}
else
  echo "Database files found in ${OUT_DIR}"
fi

# Load files in database_files
mysql -u "${USER_NAME}" -p"${PW}" -e "DROP DATABASE ${DB_NAME};"

# Clinical evidence data
mysql -u "${USER_NAME}" -p"${PW}" -e "CREATE DATABASE ${DB_NAME};"
mysql -u "${USER_NAME}" -p"${PW}" -e "USE ${DB_NAME};"
mysql -u "${USER_NAME}" -p"${PW}" -D ${DB_NAME} -e "create table clinical_evidence (molecular_profile varchar(100),molecular_profile_id varchar(10),disease varchar(250),doid varchar(50),phenotypes varchar(250),therapies varchar(250),therapy_interaction_type varchar(50),evidence_type varchar (30),evidence_direction varchar(50),evidence_level varchar(10),significance varchar(100),evidence_statement text,citation_id varchar(150),source_type varchar(30),asco_abstract_id varchar(50),citation varchar(250),nct_ids varchar(250),rating varchar(10),evidence_status varchar(40),evidence_id varchar(10),variant_origin varchar(30),last_review_date varchar(50),evidence_civic_url varchar(100),molecular_profile_civic_url varchar(250),is_flagged varchar(30), primary key(evidence_id) );"
mysql -u "${USER_NAME}" -p"${PW}" -D ${DB_NAME} -e "load data infile \"/var/lib/mysql-files/${VERSION}-ClinicalEvidenceSummaries.tsv\" into table clinical_evidence fields terminated by '\t' enclosed by '' lines terminated by '\n' ignore 1 rows;"

# Molecular profiles
mysql -u "${USER_NAME}" -p"${PW}" -D ${DB_NAME} -e "create table molecular_profiles (molecular_profile varchar(100),molecular_profile_id varchar(10),summary text,variant_ids varchar(100),variants_civic_url text,evidence_score varchar(20),evidence_item_ids text,evidence_items_civic_url text,assertion_ids varchar(30),assertions_civic_url varchar(500),aliases varchar(150),last_review_date varchar(50),is_flagged varchar(10), primary key(molecular_profile_id) );"
mysql -u "${USER_NAME}" -p"${PW}" -D ${DB_NAME} -e "load data infile \"/var/lib/mysql-files/${VERSION}-MolecularProfileSummaries.tsv\" into table molecular_profiles fields terminated by '\t' enclosed by '' lines terminated by '\n' ignore 1 rows;"

# Gene summaries
mysql -u "${USER_NAME}" -p"${PW}" -D ${DB_NAME} -e "create table gene_summaries (gene_id varchar(10),gene_civic_url varchar(100),name varchar (15),entrez_id varchar(10),description text,last_review_date varchar(50),is_flagged varchar(10), primary key(gene_id) );"
mysql -u "${USER_NAME}" -p"${PW}" -D ${DB_NAME} -e "load data infile \"/var/lib/mysql-files/${VERSION}-GeneSummaries.tsv\" into table gene_summaries fields terminated by '\t' enclosed by '' lines terminated by '\n' ignore 1 rows;"

# Create indices
mysql -u "${USER_NAME}" -p"${PW}" -D ${DB_NAME} -e "create index ce_index on clinical_evidence (molecular_profile);"

