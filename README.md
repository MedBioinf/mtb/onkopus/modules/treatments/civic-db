# CIViC-DB

Module for retrieving data from the CIViC database [1].

## Setup 

To download and set up the database, start the container and log in with
```commandline
docker exec -it civic-db /bin/bash
```
and run the preprocessing script within the container
```commandline
/mnt/preprocess.sh
```

To start the container with an already built database, mount the database files in /var/lib/mysql within the Docker container, e.g.
```commandline
docker run -it -v /local/path/to/database:/var/lib/mysql --name civic-db civic-db
```

References 

[1] Griffith, M., Spies, N.C., Krysiak, K., McMichael, J.F., Coffman, A.C., Danos, A.M., ..., Griffith, O. L. (2017). CIViC is a community knowledgebase for expert crowsourcing the clinical interpretation of variants in cancer. Nature genetics, 49(2), 170-174. 
