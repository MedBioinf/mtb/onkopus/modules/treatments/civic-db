FROM mariadb:10.11.2-jammy

RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install -y wget

COPY ./preprocess.sh /docker-entrypoint-initdb.d/preprocess.sh

EXPOSE 10104

